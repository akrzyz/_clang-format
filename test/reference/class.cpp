namespace AAA
{
namespace BBB
{

class MyClass
{
public:
    ~MyClass(){};
    MyClass(){};
    MyClass(const MyClass &) = default;
    MyClass(int x, int y);
    MyClass(int x, int y, int z);

    void foo();
    int bar(int x, int y);
    virtual int x() const
    {
        return _x;
    }

private:
    int _x;
    int _y;
    int _z;
};

MyClass::MyClass(int x, int y) : _x(x), _y(y)
{
    _z = _x + _y;
}

MyClass::MyClass(int looooooooong_x, int looooooooong_y, int looooooooong_z)
: _x(looooooooong_x), _y(looooooooong_y), _z(looooooooong_z)
{
}

MyClass::MyClass(
    int looooooooong_x,
    int looooooooong_y,
    int looooooooong_z,
    int looooooooong_q,
    int looooooooong_w,
    int looooooooong_u)
: _x(looooooooong_x), _y(looooooooong_y), _z(looooooooong_z)
{
}

MyClass::MyClass(
    int looooooooooong_x,
    int looooooooooong_y,
    int looooooooooong_z,
    int looooooooooong_q,
    int looooooooooong_w,
    int looooooooooong_u)
: _x(loooooooooooong_x),
  _y(loooooooooooong_y),
  _z(loooooooooooong_z),
  _q(loooooooooooong_q),
  _w(loooooooooooong_w),
  _u(loooooooooooong_u)
{
}

void MyClass::foo()
{
    x += _y * _y;
}

int MyClass::bar(int x, int y)
{
    return _x + _y;
}

} // AAA
} // BBB
