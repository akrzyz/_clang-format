
int i = 5;

if(i > 10){return ++i;}
else{return i++;}

std::vector<int> v;
for(int i = 0; i < v.size(); i++){v[i]=i+i;}

for(auto & i : v){i=i*i;}

switch(i){
    case 0: { return 0; }
    case 2: { return 2; }
    case 3: /* no scope */ return 3;
    default : { return 8; }
}

return i > 10
?
2 
:
4;

return (i > 10
        ? 2 : 4);
