#!/bin/bash
FORMATED_DIR="./formated"
MESSY_DIR="./messy"
REFERENCE_DIR="./reference"
CLANG_FORMAT="clang-format"
GREEN="\e[1;32m"
RED="\e[1;31m"
COLOR_END="\e[0m"

OK=0
FAIL=1
RESULT=$OK

command_exists () {
    type "$1" &> /dev/null ;
}

function test_format ()
{
    FILE=$1
    $CLANG_FORMAT -i $FORMATED_DIR/$FILE

    DIFF=`diff $REFERENCE_DIR/$FILE $FORMATED_DIR/$FILE`

    if [[ -z $DIFF ]]; then
        echo -e $GREEN $FILE PASS $COLOR_END
    else
        echo -e $RED $FILE FAILED $COLOR_END
        echo "$DIFF"
        RESULT=$FAIL
    fi
}

if ! command_exists $CLANG_FORMAT ; then
    CLANG_FORMAT="clang-format-3.8"
fi

if ! command_exists $CLANG_FORMAT ; then
    echo -e $RED $CLANG_FORMAT does not exist $COLOR_END
    exit $FAIL
fi

if [ ! -d $FORMATED_DIR ]; then
    mkdir $FORMATED_DIR
fi

#preparation
cp $MESSY_DIR/* $FORMATED_DIR/

#execution
FILES=`ls $FORMATED_DIR`
for file in $FILES; do
    test_format $file
done

#cleanup
rm $FORMATED_DIR/*

exit $RESULT
