# clang format configuration
based on LLVM style clang-format 3.8


\#commented settings are irrelevant or i have no idea what's that doing

# missing
wrap in braces single line control statement

```
#!c++

if(a)
{
    b();
}
```

#vim integration
https://github.com/rhysd/vim-clang-format

#how to
```
cd $DIR
clone https://akrzyz@bitbucket.org/akrzyz/_clang-format.git
cd $PROJECT_DIR
ln -s $DIR/_clang-format/_clang-format _clang-format
```